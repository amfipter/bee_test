import numpy as np
import scipy.spatial.distance as ssd
from sklearn.preprocessing import Imputer
import math
import re


from sklearn.preprocessing import normalize, scale

# Support functions 
class vec_func:
    pca = 0
    def devide_hex_values(data, flag = True):
        print("DEVIDE HEX")
        hex_map = list()
        for i in range(len(data[0])):
            try:
                t = float(data[0][i])
            except ValueError:
                hex_map.append(i)

        for i in range(len(data)):
            new_data = list()
            for j in range(len(data[i])):
                if j in hex_map:
                    if data[i][j].find('.') >=0:
                        print(data[i][j])

                    if len(data[i][j]) < 5:
                        data[i][j] = "0x0"
                        continue



                    t = [data[i][j]]
                    if flag:
                        chunks, chunk_size = len(data[i][j]), 2
                        t = [ data[i][j][i1:i1+chunk_size] for i1 in range(0, chunks, chunk_size) ]
                    t = map(lambda x: "0x"+x, t)
                    new_data.extend(t)
                else:
                    new_data.append(data[i][j])
            data[i] = new_data


    def normalize_data(data, flag = False, n = 3):
        print("NORMALIZE DATA")
        c = 0
        # pattern_16 = re.compile('a|b|c|d|e|f')
        # pattern_float = re.compile('\.')
        for i in range(len(data)):
            # print(i)
            while len(data[i]) < len(data[0]):
                data[i].append('0')
            for j in range(len(data[i])):
                # c += 1

                # print(data[i][j], type(data[i][j]), c)

                if data[i][j] == '':
                    data[i][j] = np.nan
                    continue

                try:
                    t = float(data[i][j])
                    # if t == float('Inf'):
                    #     print("FUCK", data[i][j])                       #little test
                    #     exit()
                    data[i][j] = t
                except ValueError:
                    t = data[i][j].split('x')
                    # print(t, data[i][j])
                    if len(t) == 1:
                        # print(data[i][j])
                        data[i][j] = int(t[0], 16)

                    else:
                        # print(data[i][j])
                        # print(data[i][j])
                        data[i][j] = int(t[1], 16)

        data = vec_func.vector_restoration(data, n)
        # print(data)
        # print(data[0])

        if flag:
            vertical_data = list()
            for i in range(len(data[0])):
                vertical_data.append(list())

            for i in range(len(data)):
                for j  in range(len(data[i])):
                    vertical_data[j].append(data[i][j])

            for i in range(len(vertical_data)):
                vertical_data[i] = np.array(vertical_data[i]).astype(float)

            print(vertical_data[61])
            for i in range(len(vertical_data)):
                # print(i, len(vertical_data))
                vertical_data[i] = normalize(vertical_data[i])[0]
                vertical_data[i] = scale(vertical_data[i])

            # print (len(vertical_data[2]))
            # print (vertical_data[0])
            # exit()

            for i in range(len(data)):
                for j  in range(len(data[i])):
                    # print(i, j, len(data), len(data[i]))
                    # print(len(vertical_data[j][i]))
                    data[i][j] = vertical_data[j][i]
        return data


    def create_clusters(data, clusters, cluster_destination):
        for el in range(7):
            cluster_destination.append(list()   )
        print(set(clusters))

        for i in range(len(clusters)):
            cluster_destination[clusters[i]].append(data[i])

    def calculate_std(data_clusters):
        std = list()
        for i in range(len(data_clusters)):
            matrix = np.matrix(data_clusters[i]);
            t = np.std(matrix, 0)
            std.append(t.tolist())
        return std

    def calculate_mean_vectors(data_clusters):
        output = list()
        for e in data_clusters:
            print(len(e))
            vec = np.array(e[0])
            vec = vec.astype(float)
            for i in range(1, len(e)):
                try:
                    vec += e[i]
                except Exception as ex:
                    print(len(e[i]), len(vec), ex)
                    exit()

            vec /= len(e)
            output.append(vec)
        return output

    def calculate_minmaxmean_distance(data_clusters, mean_clusters):
        dist_inside_cluster = list()
        dist_outside_cluster = list()

        for i in range(len(data_clusters)):             # range: clusters
            dist_inside_cluster.append(list())
            dist_outside_cluster.append(list())
            for j in range(len(data_clusters[i])):      #range: vectors in clusters
                dist_inside_cluster[i].append(vec_func.distance(data_clusters[i][j], mean_clusters[i]))

            for k in range(len(data_clusters)):         #range: all other clusters
                if k == i:
                    continue
                for j in range(len(data_clusters[k])):  #range vectors in clusters
                    dist_outside_cluster[i].append(vec_func.distance(data_clusters[k][j], mean_clusters[i]))

        for i in range(len(dist_inside_cluster)):
            dist_inside_cluster[i] = np.array(dist_inside_cluster[i])
            dist_outside_cluster[i] = np.array(dist_outside_cluster[i])


        print("dist info:\nMIN\t\tMAX\t\tMEAN\n")
        for i in range(len(data_clusters)):
            print("CLUSTER " + str(i) + ":")
            print(dist_inside_cluster[i].min(), dist_inside_cluster[i].max(), dist_inside_cluster[i].mean())
            print(dist_outside_cluster[i].min(), dist_outside_cluster[i].max(), dist_outside_cluster[i].mean())

    def distance(u, v):
        out = 0
        # print("TEST", vec_func.pca)
        pca_vec = vec_func.pca.explained_variance_ratio_
        if len(u) != len(v):
            raise Exception("Incorrect vector sizes")

        # print(len(v), len(pca_vec))
        for i in range(len(u)):
            out += ((float(v[i]) - float(u[i]))* pca_vec[i])**2

        out = out**(1.0 / 2)
        # out = ssd.euclidean(u, v)

        # out = ssd.correlation(u, v)
        # out = ssd.sqeuclidean(u, v)
        return out

    def test_clusters(data, clusters, answer):
        correct = 0
        for i in range(len(data)):
            dist = list()
            for c in clusters:
                dist.append(vec_func.distance(data[i], c))
            cluster = np.array(dist).min()
            cluster = dist.index(cluster)
            if cluster == answer[i]:
                correct += 1
        return float(correct) / len(answer)

    def list_to_np(list_array):
        pass

    def vector_restoration(data, n = 3):
        if n == 0:
            # for i in range(len(data)):
            #     for j in range(len(data[i])):
            #         if data[i][j] == float("Inf"):
            #             data[i][j] = 0
            np_data = np.array(data)
            imp = Imputer(missing_values="NaN", strategy='mean', axis=0)
            np_data = imp.fit_transform(np_data)
            data = list(np_data)
            return data
        print("RESTORE PROCESS:")
        data_clear = list()
        data_missed = list()

        for v in data:
            if np.nan in v:
                data_missed.append(v)
            else:
                data_clear.append(v)
        print(len(data_clear))
        print(len(data_missed))
        c = 0
        # data_missed = np.array(data_missed_)
        # data_clear = np.array(data_clear_)
        # data_ = np.array(data)
        for i in range(len(data)):
            if data[i] in data_clear:
                continue
            c += 1
            print(float(c) / len(data_missed))

            missing_points = list()
            for j in range(len(data[i])):
                if data[i][j] == np.nan:
                    data[i][j] = 0
                    missing_points.append(j)

            nearest_vectors = list()
            for k in range(len(data_clear)):
                dist = 0
                for j in range(len(data_clear[k])):
                    if j in missing_points:
                        continue
                    dist += (float(data_clear[k][j]) - float(data[i][j]))**2
                dist = dist**(1.0 / 2)
                nearest_vectors.append((dist, k))

            nearest_vectors.sort(key=lambda tup: tup[0])
            # print(nearest_vectors)
            # exit()
            nearest_vectors = nearest_vectors[:n]

            for k in range(n):
                for j in missing_points:
                    data[i][j] += data_clear[nearest_vectors[k][1]][j]

            for j in missing_points:
                data[i][j] /= n
        return data

    def transform_bad_data(in_data):
        data_ = list()
        for i in range(len(in_data)):
            # print(len(train_data_n[i]))
            for j in range(len(in_data[i])):
                try:
                    data_.append(float(in_data[i][j]))
                except Exception:
                    t = re.search('^(-?\d\.\d+)(-?\d\.\d+)$', in_data[i][j])
                    data_.append(float(t.group(1)))
                    data_.append(float(t.group(2)))
                    # print(j, train_data_n[i][j])
                    # if j > 2000:
                    #     exit()
        train_data_n = list()
        for i in range(50000):
            train_data_n.append(list())
            for j in range(138):
                train_data_n[i].append(data_[i*138 + j])
            # print(len(train_data_n[i]))
        return train_data_n






