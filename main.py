#!/usr/bin/env python3
import re
import numpy as np
import scipy.spatial.distance as ssd
from sklearn.preprocessing import normalize, scale
from data_func import vec_func
from sklearn.decomposition import PCA

from sklearn import metrics, svm
from sklearn.ensemble import ExtraTreesClassifier, RandomForestClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.grid_search import RandomizedSearchCV, GridSearchCV
from scipy.stats import uniform as sp_rand
from sklearn.linear_model import Ridge
from sklearn.naive_bayes import GaussianNB
from sklearn.neighbors import KNeighborsClassifier
import re



with open('train.csv') as f:
    data = f.readlines()

def parse_line(line):
    out = line.strip().split(',')
    return out

with open('train_restored.csv') as f:
    data_t = f.readlines()

with open('test_restoreed.csv') as f:
    data_r = f.readlines()


train_data = list()
train_data_n = list()

train_ans  = list()

test_data = list()
test_data_n = list()

train_clusters = list()
mean_clusters  = None

data_len = len(parse_line(data.pop(0))) - 1
# print(data_len)
# print(data[0])



for e in data:
    train_data.append(parse_line(e))

for e in data_t:
    train_data_n.append(parse_line(e))

for e in data_r:
    test_data_n.append(parse_line(e))

with open('test.csv') as f:
    data = f.readlines()

for e in data:
    test_data.append(parse_line(e))

test_data.pop(0)
# train_data_n.pop(0)
# test_data_n.pop(0)

for i in range(len(test_data)):
    test_data[i].pop(0)

# for i in range(len(test_data_n)):
#     test_data_n[i].pop(-1)

# print(test_data[0])

for i in range(len(train_data)):
    train_ans.append(int(train_data[i].pop()))
# print('1')

train_data.pop(-1)
test_data.pop(-1)
train_ans.pop(-1)

vec_func.devide_hex_values(train_data, True)
vec_func.devide_hex_values(test_data, True)


# train_data_n = vec_func.transform_bad_data(train_data_n)
train_data_n = vec_func.normalize_data(train_data_n, False, 0)
train_data = vec_func.normalize_data(train_data, False, 0)
test_data_n = vec_func.normalize_data(test_data_n, False, 0)
test_data = vec_func.normalize_data(test_data, False, 0)
# print(train_data[0])
# READY
# diff = np.array(train_data) - np.array(train_data_n)
# print(diff)

# exit()

dataX = np.array(train_data_n)
dataY = np.array(train_ans)
data_test = np.array(test_data_n)

dataX = normalize(dataX)
dataX = scale(dataX)

data_test = normalize(data_test)
data_test = scale(data_test)

# model = ExtraTreesClassifier()
model = DecisionTreeClassifier()
# model = Ridge(alpha=0.243448311812)
# model = svm.SVC(kernel='rbf', C=2.0)
# model = GaussianNB()
# model = RandomForestClassifier()

# param_grid = {'n_estimators' : [5, 20, 40, 80, 160], 'criterion': ('gini', 'entropy'), 'max_features': ('sqrt', 'log2', None)}

# param_grid = {'C':[2.0, 2.05, 2.1, 2.15]}
# param_grid = {'alpha': sp_rand()}

# rsearch = RandomizedSearchCV(estimator=model, param_distributions=param_grid, n_iter=100)
# rsearch = GridSearchCV(model, param_grid, n_jobs=4)
# rsearch.fit(dataX, dataY)
# print(rsearch.best_score_)
# print(rsearch.best_estimator_.kernel, rsearch.best_estimator_.C)

# exit()

print("MODEL")
model.fit(dataX, dataY)
predict = model.predict(dataX)
predict_test = model.predict(data_test)
print("==========")
# print(metrics.classification_report(dataY, predict))
# print(metrics.confusion_matrix(dataY, predict))

c = 0
for i in range(len(predict)):
    if predict[i] == dataY[i]:
        c += 1

print(float(c) / len(dataY))

with open('sol.csv', 'w') as f:
    f.write("ID,y\n")
    for i in range(len(predict_test)):
        f.write(str(i) + ',' + str(predict_test[i]) + "\n" )


# print(model.feature_importances_)



# pca = PCA(n_components=len(train_data[0]))
# pca.fit(np.array(train_data))
# vec_func.pca = pca

# print(train_data[0])
# exit()
# vec_func.create_clusters(train_data, train_ans, train_clusters)
# mean_clusters = vec_func.calculate_mean_vectors(train_clusters)


# print("PCA", pca.components_)
# vec_func.calculate_minmaxmean_distance(train_clusters, mean_clusters)
#
# print(vec_func.test_clusters(train_data, mean_clusters, train_ans))

# print("mean clusters:", mean_clusters)
#
# print(train_data[0])