import numpy as np

class data_vec:
    def __init__(self, str_data):
        self.raw_data = str_data
        self.hex_data = list()
        self.float_data = list()
        self.mixed_data = list()
        self.hex_map = None

    def get_hex_map(self):
        hex_map = list()
        for i in range(len(self.raw_data)):
            try:
                t = float(self.raw_data[i])
            except ValueError:
                hex_map.append(i)
        return hex_map

    def parse_data(self, devide_hex_flag = False):
        if self.hex_map is None:
            raise Exception("Hex map not defined")
        for i in range(len(self.raw_data)):
            if i in self.hex_map:
                self.hex_data.append(self.raw_data[i])
            else:
                self.float_data.append(self.raw_data[i])

        for i in range(len(self.hex_data)):
            self.hex_data[i] = int(self.hex_data[i], 16)

        for i in range(len(self.float_data)):
            self.float_data[i] = float(self.float_data[i])


