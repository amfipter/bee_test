#!/usr/bin/env python3
from data_func import vec_func
import copy

with open('train.csv') as f:
    data = f.readlines()

title = data.pop(0)

# with open('train010.csv', 'w') as f:
#     f.write(title)
#     for i in range(0, len(data), 10):
#         f.write(data[i])

def parse_line(line):
    out = line.strip().split(',')
    return out

train_data = list()

data_len = len(parse_line(title)) -1

for e in data:
    train_data.append(parse_line(e))

train_data_restored3 = copy.deepcopy(train_data)

vec_func.devide_hex_values(train_data_restored3, True)
vec_func.normalize_data(train_data_restored3, True, 1000)

with open("train_norm_restored1000.csv", 'w') as f:
    f.write(title)
    for v in train_data_restored3:
        out = ''
        for i in range(len(v)):
            out += str(v[i]) + ','
        f.write(out[:-1])
        f.write("\n")

