#include <vector>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <limits>
#include <cstring>
#include <string>
#include <cmath>

using namespace std;

inline bool in_vector(vector<int>& in, int val) {
    // bool out = false;
    int in_size = in.size();
    for(int i=0; i<in_size; i++) {
        if(in[i] == val) {
            return true;
        }
    }
    return false;
}

vector<vector<double> > n_nearest(vector<vector<double> > all_vec, vector<double> dist, int n) {
    vector<int> index;
    // printf("%d\n", __LINE__);
    for(int i=0; i<n; i++) {
        double min = 100500.0;
        int index_t = -1;
        for(int j=0; j<dist.size(); j++) {
            // printf("%f %f\n", dist[j], min);
            if(dist[j] < min) {
                // printf("%d\n", __LINE__);
                min = dist[j];
                index_t = j;
            }    
        }
        dist[index_t] = 100500.0;
        index.push_back(index_t);
    }
    // printf("%d\n", __LINE__);
    vector<vector<double> > out;
    for(int i=0; i<index.size(); i++) {
        // printf("%d %d\n", __LINE__, index[i]);
        out.push_back(all_vec[index[i]]);
    }
    // printf("%d\n", __LINE__);
    return out;

}

vector<double> find_dist(vector<vector<double> >& src, vector<double>& target, vector<int>& missing) {
    vector<double> dist;
    for(int i=0; i<src.size(); i++) {
        double dist_t = 0.0;
        for(int j=0; j<src[i].size(); j++) {
            if(in_vector(missing, j)) {
                continue;
            }
            dist_t += (target[j] - src[i][j]) * (target[j] - src[i][j]);
        }
        // printf("%d %f\n", __LINE__, dist_t);
        dist_t = sqrt(dist_t);
        // printf("%d %f\n", __LINE__, dist_t);
        dist.push_back(dist_t);
    }
    return dist;
}

void repair_vector(vector<double>& target, vector<vector<double> >& nearest, vector<int> missing) {
    int n = nearest.size();
    for(int i=0; i<target.size(); i++) {
        if(!in_vector(missing, i)) {
            continue;
        }
        target[i] = 0.0;
    }
    for(int i=0; i<n; i++) {
        for(int j=0; j<target.size(); j++) {
            if(!in_vector(missing, j)) {
                continue;
            }
            target[j] += nearest[i][j];
        }
    }
    for(int i=0; i<target.size(); i++) {
        if(!in_vector(missing, i)) {
            continue;
        }
        target[i] /= n;
    }

}

int main(int argc, char** argv) {
    char* filename = argv[1];
    int n = atoi(argv[2]);
    char t[20];
    double s = numeric_limits<double>::quiet_NaN();
    vector<vector<double> > clear_data;
    vector<vector<double> > dirty_data;
    //std::istringstream iss("1.0 -NaN inf -inf NaN 1.2");
    //double_istream in(iss);
    ifstream file(filename);
    bool flag = true;
    int i = 0;
    int j = 0;
    vector<double> t_vec(135);
    printf("%d\n", __LINE__);
    while(file >> t) {
        if(strcmp(t, "dirty") == 0) {
            cout << t;
            flag = false;
            continue;
        }
        if(t != "NaN") {
            s = atof(t);
        } else {
            s = numeric_limits<double>::quiet_NaN();
        }
        t_vec[i] = s;
        i += 1;
        if(i == 135) {
            i = 0;
            if(flag) {
                clear_data.push_back(t_vec);
            } else {
                dirty_data.push_back(t_vec);
            }
        }


    }
    printf("%d\n", __LINE__);
    int vec_size = clear_data[0].size();
    for(int i=0; i<vec_size; i++) {
        cerr << clear_data[0][i] << ' ';
    }
    const double nan_ = numeric_limits<double>::quiet_NaN();
    cout << endl;

    for(int i=0; i< dirty_data.size(); i++ ) {
        printf("\r%f%%", static_cast<float>(i) / dirty_data.size() * 100);
        vector<int> missing;
        // printf("%d\n", __LINE__);

        for(int j=0; j<vec_size; j++) {
            // printf("%d %d\n", __LINE__, j);
            if(isnan(dirty_data[i][j])) {
                missing.push_back(j);
            }
        } 
        // printf("%d\n", __LINE__);

        vector<double> dist = find_dist(clear_data, dirty_data[i], missing);
        // printf("%d\n", __LINE__);
        vector<vector<double> >nearest = n_nearest(clear_data, dist, n);
        // printf("%d\n", __LINE__);
        repair_vector(dirty_data[i], nearest, missing);
    }
    printf("%d\n", __LINE__);
    strcat(filename, "_out");
    ofstream file_out(filename);
    printf("%d\n", __LINE__);
    for(int i=0; i<clear_data.size(); i++) {
        for (int j=0; j < clear_data[i].size(); j++) {
            file_out << clear_data[i][j] << " ";
        }
        file_out << endl;
    }
    // file_out << "dirty" << endl;

    for(int i=0; i<dirty_data.size(); i++) {
        for (int j=0; j < dirty_data[i].size(); j++) {
            file_out << dirty_data[i][j] << " ";
        }
        file_out << endl;
    }

    file_out.close();


    cout << clear_data.size() << endl;
    cout << dirty_data.size() << endl;
    // cin >> t;
    
    // cout << s << endl;
}