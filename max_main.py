#!/usr/bin/env python3
import re
import numpy as np
import scipy.spatial.distance as ssd
from sklearn.preprocessing import normalize, scale
from data_func import vec_func
from sklearn.decomposition import PCA

from sklearn import metrics, svm
from sklearn.ensemble import ExtraTreesClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.grid_search import RandomizedSearchCV, GridSearchCV
from scipy.stats import uniform as sp_rand
from sklearn.linear_model import Ridge
import re


with open('train.csv') as f:
    data = f.readlines()


def parse_line(line):
    out = line.strip().split(',')
    return out

train_data = list()

train_ans = list()

test_data = list()

train_clusters = list()
mean_clusters = None

data_len = len(parse_line(data.pop(0))) - 1
# print(data_len)
# print(data[0])


for e in data:
    train_data.append(parse_line(e))


with open('test.csv') as f:
    data = f.readlines()

for e in data:
    test_data.append(parse_line(e))

test_data.pop(0)

for i in range(len(test_data)):
    test_data[i].pop(0)

print(test_data[0])

for i in range(len(train_data)):
    train_ans.append(int(train_data[i].pop()))

vec_func.devide_hex_values(train_data, True)
vec_func.devide_hex_values(test_data, True)

train_data = vec_func.normalize_data(train_data, True, 0)
test_data = vec_func.normalize_data(test_data, True, 0)
print(train_data[0])
# READY

dataX = np.array(train_data)
dataY = np.array(train_ans)
data_test = np.array(test_data)

dataX = normalize(dataX)
dataX = scale(dataX)

data_test = normalize(data_test)
data_test = scale(data_test)

model = svm.SVC()

param_grid = {'kernel':('rbf', 'poly'), 'C':[1, 3, 5, 7]}

rsearch = GridSearchCV(model, param_grid, n_jobs=8)
rsearch.fit(dataX, dataY)
print(rsearch.best_score_)
print(rsearch.best_estimator_.alpha)

exit()