#!/usr/bin/env python3
from data_func import vec_func
import numpy as np
import copy

print('recover test data')

with open('test.csv') as f:
    data = f.readlines()

title = data.pop(0)

def parse_line(line):
    out = line.strip().split(',')
    return out

train_data = list()

data_len = len(parse_line(title)) -1

for e in data:
    train_data.append(parse_line(e))

for i in range(len(train_data)):
    train_data[i].pop(0)

train_data_restored3 = train_data # copy.deepcopy(train_data)

vec_func.devide_hex_values(train_data_restored3, True)
vec_func.normalize_data(train_data_restored3, False, 0)

data_clear = list()
data_dirty = list()



print(len(data_clear))
print(len(data_dirty))
# print(len(data_clear[0]))

with open('test1', 'w') as f:
    for v in train_data:
        for el in v:
            f.write(str(el) + ' ')
        f.write("\n")

exit()


with open('train010_dirty', 'w') as f:
    for v in data_clear:
        for el in v:
            f.write(str(el) + ' ')
        f.write("\n")
    f.write("dirty\n")
    for v in data_dirty:
        for el in v:
            f.write(str(el) + ' ')
        f.write("\n")

exit()

with open("train_norm_001.csv", 'w') as f:
    f.write(title)
    for v in train_data_restored3:
        out = ''
        for i in range(len(v)):
            out += str(v[i]) + ','
        f.write(out[:-1])
        f.write("\n")


exit()


train_data_restored5 = copy.deepcopy(train_data)

vec_func.devide_hex_values(train_data_restored5, True)
vec_func.normalize_data(train_data_restored5, True, 5)

with open("test_norm_restored5.csv", 'w') as f:
    f.write(title)
    for v in train_data_restored5:
        out = ''
        for i in range(len(v)):
            out += str(v[i]) + ','
        f.write(out[:-1])
        f.write("\n")

train_data_restored7 = copy.deepcopy(train_data)

vec_func.devide_hex_values(train_data_restored7, True)
vec_func.normalize_data(train_data_restored7, True, 7)

with open("test_norm_restored7.csv", 'w') as f:
    f.write(title)
    for v in train_data_restored7:
        out = ''
        for i in range(len(v)):
            out += str(v[i]) + ','
        f.write(out[:-1])
        f.write("\n")


